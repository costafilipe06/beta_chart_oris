//Pour un accès plus facile à cette variable
isDebug = true;


/////////////////////
/// Début Ready() ///
/////////////////////
$(function(){
  //Détecter version mobile
  if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)){
    USER_INFOS.is_mobile = true;
    $("body").addClass("is-mobile");
  }

  //////////////////////////////
  /// Configuration ToastrJS ///
  //////////////////////////////
  toastr.options.timeOut = 1500; // How long the toast will display without user interaction
  toastr.options.progressBar = true;
  toastr.options.preventDuplicates = true;
  toastr.options.positionClass = "toast-top-left";
  
  ///////////////////////////////////
  /// Récupération des Paramètres ///
  ///////////////////////////////////
  try {
    getParams(window.location.search); //Initialise l'objet global parametres_url
  } catch (e) {
    //Erreur connue
    if (GLOBAL_ERROR_MESSAGE.length > 0) {
      console.error(GLOBAL_ERROR_MESSAGE);
      $("#root").prepend(new BootstrapAlert("danger", "Erreur:", GLOBAL_ERROR_MESSAGE).div)
    } else { //Autre erreur
      console.error(e);
      $("#root").prepend(new BootstrapAlert("danger", "Erreur:", e).div)
    }
    
    hideLoading();
    return false;
  }
  //Active ou désactive la légende du graphique
  var legendEnabled = (parametres_url.name && parametres_url.name.length > 0) ? true : false;
  console.warn("legendEnabled: ", legendEnabled);

  //Activer/désactiver axes logarithmics (booléen commun pour tous les axes)
  var logarithmicYaxis = (parametres_url.log == "true") ? "logarithmic" : "linear";
  console.warn("logarithmicYaxis: ", logarithmicYaxis);

  //Activer/désactiver le type polar
  var ispolar = (parametres_url.ispolar == "true") ? true : false;
  console.warn("ispolar: ", ispolar);

  ////////////////////////////////
  /// Configuration HighCharts ///
  ////////////////////////////////
  //Les événements des clics définis ici seront généraux 
  Highcharts.setOptions({
    //UTC dynamique en fonction du client
    time: {
      timezoneOffset: new Date().getTimezoneOffset()
    },
    chart: {
      polar: ispolar
    },
    xAxis: {
      maxPadding: 0.02,
      minPadding: 0.02
    },
    yAxis: {
      maxPadding: 0.05,
      minPadding: 0.05,

      type: logarithmicYaxis
    },
    plotOptions: {
      series: {
        animate: true
      }
    },
    legend: {
      enabled: legendEnabled
    }
  });

  //La langue par défaut est la langue de Molière fr-FR
  if (window.navigator.language != "en" && window.navigator.language != "en-US"){
    Highcharts.setOptions({
      lang: GLOBAL_LANG.fr,
    });
    configAvecTableau.tooltip.xDateFormat = GLOBAL_LANG.fr.xDateFormat;
  } else 
    console.warn("Highcharts language: 'en'");
  
  //SVG Custom (plus) support IE je crois
  if (Highcharts.VMLRenderer) {
    Highcharts.VMLRenderer.prototype.symbols.ajouterCourbe = Highcharts.SVGRenderer.prototype.symbols.ajouterCourbe;
  }

  //Init le premier graphe
  appelant = initEmptyNewGraph( '95%', 'auto', configAvecTableau, "");

  ////////////////////////////////////////////////////////
  //////// Initialiser avec les paramètres de URL ////////
  ////////////////////////////////////////////////////////
  var initComplete = trueInit(appelant);    
  if (!initComplete)
    $("#root").prepend(new BootstrapAlert("danger", "Erreur:", GLOBAL_ERROR_MESSAGE).div)

  
  //On signale que la page a finit de chargé 
  isInitialisationFinished = true;
  //hideLoading();

}); //fin ready()

/**
 * Supprime une série
 * sa ligne dans la table d'analytiques
 * et son marqueur sur le label de l'axe Y
 * @param  {Highcharts object} oSerie    la série à supprimer
 * @return {[type]}        [description]
 */
function supprimerSerie(oSerie){
  //Enlever les références des picks etc....
  //Supprimer la ligne du tableau d'analytiques
  supprimerLigneById(oSerie.chart.renderTo.id, oSerie.userOptions.id);
  /* Retirer le marker (couleur) du label de l'axe Y */
  supprimerMarkerLabelY(oSerie);

  //Si jamais il s'agissait de la dernière série de l'axe, on le supprime également
  if( oSerie.yAxis.series.length < 2 ) //0 == pas de série. 1 == "dernière série"
    oSerie.yAxis.remove();
  else    
    oSerie.remove(); //Supprimer la courbe
}

function yAxisMarkerColorUpdate(oSerie, nouvelleCouleur){
  nouvelleCouleur = nouvelleCouleur || oSerie.color || COULEURS_AUTO[oSerie.index];
  if(!nouvelleCouleur)
    return false;

  var yAxis = oSerie.yAxis;
  $(yAxis.axisTitle.element)
    .find("i#label-marker-" + oSerie.userOptions.id)
    .first().css("color", nouvelleCouleur)
    .addClass(SYMBOLS_AUTO_FA_EQUIVALENT[oSerie.symbol]);

   return yAxis.update( { title: { text: yAxis.axisTitle.element.innerHTML } } );  
}

/**
 * Recherche si un yAxis au label égal au paramètre existe dans le graphe
 * Utiliser cette fonction pour cherche si un yAxis pour une unité donnée existe déjà ou non
 * @param  {string} recherche        label ("unité") recherché
 * @return {int}                    index de l'yAxis s'il a été trouvé, 
 *                                   ou -1
 **/
function getYaxisByText(recherche, _chart){
   _chart = _chart || appelant;
   //console.error("getYaxisByText recherche:", recherche);

   //console.info("Les yAxis de l'appelant:");
   var i = _chart.yAxis.length;
   while ( i-- ) {
     //console.info("i", i);
     if(_chart.yAxis[i].axisTitle  
       && ( recherche == _chart.yAxis[i].axisTitle.element.innerText
          || recherche == _chart.yAxis[i].axisTitle.textStr ) ){  //Éviter l'erreur "axisTitle is undefined" si jamais on utilise le navigator alligator
      //console.info("_chart.yAxis[i].axisTitle.innerText: ", _chart.yAxis[i].axisTitle.element.innerText);
       //console.info("_chart.yAxis[i].axisTitle.textStr: ", _chart.yAxis[i].axisTitle.textStr);
       return i;
     }
     if(_chart.yAxis[i].userOptions && _chart.yAxis[i].userOptions.title){
     //console.info("_chart.yAxis[i].userOptions.title.text: ", _chart.yAxis[i].userOptions.title.text);     
       if( recherche == _chart.yAxis[i].userOptions.title.text ){
         return i;
       }
     }
   }
   return -1;
}

/** 
 * Crée une div destinée à contenir un graphique
 * @param  {string}       width    largeur, avec unité, de la div à créer
 * @param  {string}       height   hauteur, avec unité, de la div à créer
 * @return {DOM Element}          l'enfant de la div créée (la div créée regroupe graphe+tableau)
 */
function createNewGraphContainer(width, height){
   //Get Config
   var containerId = "graph-" + charts_and_tables.length;
   //Déclare nouvelle liste
   tab_parametres_picks[containerId] = [];
   return $('<div id="true-' + containerId + '" class="true-container" style="width: ' + width + '; display: block; margin: 15px auto; z-index: -1; "><div id="' + containerId + '" class="my-highcharts-container " style="height: ' + height + '; " ></div></div>');
    //.append("body")[0];
}

/**
 * Crée une div et initialise un graphe avec la configuration par défaut
 * @param  {string}       width    largeur, avec unité, de la div à créer
 * @param  {string}       height   hauteur, avec unité, de la div à créer
 * @param  {JS Object}    config   (optionnel) configuration du graphe à créer 
 *                                 (par défaut, c'est la config qui crée un tableau de données en dessous)
 * @return {API Object}        le graphe créé (objet JS créé par HighCharts)
 */
function initEmptyNewGraph(width, height, config, title, callback){
  config = config || configAvecTableau;
  if(title)
    config.title = { 
      text: title || ""
    };

  graphDOM = createNewGraphContainer(width, height).first()[0];
  $("#root").append(graphDOM);
  //console.info("graphDOM", graphDOM);

  appelant = Highcharts.chart(graphDOM.id, configAvecTableau, function() {
    //creerTableDonnees(this);
    if( callback )
      callback();
  });

  //Le graphe s'initialise avec un axe "vide", ce qui nous dérange
  appelant.yAxis[0].remove();
  //appelant.showNoData();

  return appelant;
}

function trueInit(oHighChart) {
  //Récupérer les paramètres
  if (!parametres_url) {
    GLOBAL_ERROR_MESSAGE = "L'objet parametres_url n'est pas initialisé"
    return false;
  }
  if (isDebug) {
    console.log("Paramètres de l'url:");
    console.log(parametres_url);
  }

  //TITRE & SOUS-TITRE
  initTitles(parametres_url, oHighChart);

  //Créer les Séries
  series = instantiateSeries(parametres_url);
  if (!series)
    return false;
  if (isDebug) {
    console.log("Séries vides:");
    console.log(series);
  }

  //Paramétrer les séries
  configureSeries(series);
  if (isDebug) {
    console.log("Séries paramétrées:");
    console.log(series);
  }

  //Faire la requête "&json=optim" 
  //ET
  //fakeParse2 + affecter les datas aux Séries correspondantes
  //ET
  //update le graphe
  oHighChart = oHighChart || appelant;
  GET_chart_oriss(parametres_url.data, parametres_url.mask, oHighChart);

  return true;
}







/**
 * Récupère les paramètres de l'URL 
 * ET 
 * les transforme en un Objet JS
 * 
 * @return {JS Object/Boolean}                 
 *     l'Objet JS où les clés sont les paramètres de l'url et les valeurs 
 *     sont celles de ces mêmes clés 
 *   OU
 *     FALSE, s'il n'y a aucun paramètres ou s'il manque ceux obligatoires (&data &mask)
 */
function getParams(location_search) {
  //Variables
  //location_search = decodeURIComponent(location_search);
  if (!location_search || !location_search.length) {
    GLOBAL_ERROR_MESSAGE = "Pas de paramètres dans l'URL";
    throw GLOBAL_ERROR_MESSAGE;
  }
  //Récupérer les paramètres
  var parama_split = location_search.replace('?', '').split("&");//Tout derrière le 1er "?" (que l'on supprime) de l'URL --> devient un tableau
  //Stocker les paramètres comme objet
  var i = parama_split.length;
  while (i--) {
    //Ignorer les paramètres vides "&&"
    if (parama_split[i].length < 1) 
      continue;
	  
    var arr = parama_split[i].split("="); //arr[0] == clé/paramètre ("mask", "data", ...)
	
    //Ignorer les faux paramètres "&toto" ou "&tata="
    if (arr.length < 2 ||                         //paramètres sans "="
        arr[0].length < 1 || arr[1].length < 1)   //paramètres sans valeur après le "="
      continue;
    parametres_url[arr[0]] = decodeURIComponent(parama_split[i].split(/=(.+)/)[1]);
    if (!parametres_url[arr[0]]){
      GLOBAL_ERROR_MESSAGE = "Le paramètre '" + arr[0] + "' n'a pas de valeur";
      //throw GLOBAL_ERROR_MESSAGE;
    }
    //parametres_url["_" + arr[0]] = decodeURIComponent(parametres_url[arr[0]]).split(",");
    parametres_url["_" + arr[0]] = (parametres_url[arr[0]]).split(",");
  }

  //Faux si on a des paramètres mais pas les 2 OBLIGATOIRES (&data et &mask)
  //ET s'il n'y a qu'une valeur à l'attribut '&mask'
  if (!parametres_url.data || !parametres_url.mask || parametres_url._mask.length < 2) {
    if (!parametres_url.data)
      GLOBAL_ERROR_MESSAGE = "Le paramètre '&data' est manquant ou vide";
    if (!parametres_url.mask || parametres_url._mask.length < 2)
      GLOBAL_ERROR_MESSAGE = "Le paramètre '&mask' est manquant ou n'a pas assez de valeurs (2 minimum)"
    throw GLOBAL_ERROR_MESSAGE;
  }

  return parametres_url;
}

/**
 * S'ils sont précisés en paramètre, 
 * le titre et/ou le sous-titre sont affectés au graphe
 */
function initTitles(_parametres_url, oHighChart) {
  if(_parametres_url._title)
    oHighChart.update({title: { text: _parametres_url._title[0] } }, false);
  if(_parametres_url._subtitle)
    oHighChart.update({subtitle: { text: _parametres_url._subtitle[0] } }, false);
}

/**
 * Initialise le nombre nécessaire de séries en fonction de la requête
 * 
 * @return {JS Object}                 
 *     Objet JS servant de "dictionnaire" (clé/valeurs) contenant les séries à construire
 *     avec des valeurs par défaut
 */
function instantiateSeries() {
  var _series = {},
      nbmask = parametres_url._mask.length || 0;

  for (var i=1; i<nbmask; ++i) { //i=1 car le premier 'mask' est le libellé du tick de l'axeX (string ou date, voir plus tard)
    _series[parametres_url._mask[i]] = new Serie(parametres_url._mask[i]);
  }
  return _series;
}

/**
 * Configure toute une Série
 * @param  {Classe Serie} _series 
 *     Instance de la classe Série à initialiser
 * @return {Classe Serie}
 *     La Série paramétrée 
 *     (return inutile car, en JS, les Objets sont toujours des références, 
 *      même quand passé en paramètre d'une fonction)
 */
function configureSeries(_series) {
  //NAME et UNIT  
  //Array des paramètres optionnels possibles paramétrant les séries 
  //(donc pas "istime", ni "color")
  var tmpParams = ["type", "name", "unit", "color"];
  for (var p in tmpParams) {
    console.warn("configureSeries -> " + tmpParams[p], _series);
    configureParam(_series, tmpParams[p]);
  }
  for (var s in _series) {
    console.error("Couleur de la série paramétrée:", "--> " + _series[s].color);
  }
  return _series;
}

/**
 * Configure un paramètre spécifique d'une Série
 * @param  {Classe Serie} _series     
 *     Série à paramétrer
 * @param  {string} _param_name 
 *     nom du paramètre à configurer
 * @return {boolean}
 *     true si la configuration s'est correctement passée
 *     OU
 *     false si un soucis a été rencontré
 */
function configureParam(_series, _param_name) {
  var _nbSeries = Object.keys(_series).length;
  //Le paramètre existe
  if (parametres_url["_"+_param_name]) {
    var def = new Serie()[_param_name];
    //CAS SPÉCIFIQUE (TYPE): 
    //1 seule valeur => Type commun à tous => on change la valeur par défaut
    if (_param_name == "type" && parametres_url["_type"].length == 1) 
      def = parametres_url._type[0];

    //S'il y a plus de valeurs de paramètres que de Séries, on ne loop que sur le nombre de Séries
    var j = (parametres_url["_"+_param_name].length > _nbSeries) ? _nbSeries : parametres_url["_"+_param_name].length;
    if (_param_name == "type" && parametres_url["_"+_param_name].length == 1)
      j = _nbSeries;
    while (j--) {
      if (_param_name == "color") {
        console.info("color: ", "#"+parametres_url._color[j]);
        _series[parametres_url._mask[1+j]][_param_name] = "#" + parametres_url._color[j] || COULEURS_AUTO[Object.keys(series).indexOf(key) % COULEURS_AUTO.length]
                            || COULEURS_AUTO[0];
      }
      else if (_param_name == "type")    //Cas particulier paramètre "type" (on n'accepte qu'un type légal)
        _series[parametres_url._mask[1+j]][_param_name] = isAuthorizedType(parametres_url._type[j], def);
      else if (_param_name == "unit") //Cas particulier paramètre "unit" (on essaye de formatter l'unité)
        _series[parametres_url._mask[1+j]][_param_name] = 
      UNITS_CLEAN[(parametres_url["_"+_param_name][j])] 
      //UNITS_CLEAN[decodeURIComponent(parametres_url["_"+_param_name][j])] 
      || (parametres_url["_"+_param_name][j]) || ""
      //|| decodeURIComponent(parametres_url["_"+_param_name][j]) || ""
      else
        _series[parametres_url._mask[1+j]][_param_name] = (parametres_url["_"+_param_name][j]);
        //_series[parametres_url._mask[1+j]][_param_name] = decodeURIComponent(parametres_url["_"+_param_name][j]);
    }
    return true;
  } else {
    //GLOBAL_ERROR_MESSAGE = "Le paramètre '" + _param_name + "' n'existe pas";
    console.warn("Le paramètre '" + _param_name + "' n'a pas été renseigné.");
    var j = _nbSeries;
    if (_param_name == "name") {
      while (j--) {
        _series[parametres_url._mask[1+j]][_param_name] = " "; //un espace car si on met "" ou null, HighCharts met son propre nom ("Series 1", ...)
      }
    }
    return false;
  }
}

/**
 * Vérifie qui le type de la série est possible (existe dans HighCharts)
 * @param  {String}  _type
 *     type de la série
 * @param  {String}  _def 
 *     valeur par défaut
 * @return {String}
 *     la valeur correcte, acceptée par HighCharts, ou par défaut
 */
function isAuthorizedType(_type, _def) {
  console.info(_def)
  if (Highcharts.seriesTypes[_type])
    return _type;
  console.warn("Le type '" + _type + "' n'est pas reconnu. '" 
                + _def + "' est utilisé à la place" );
  return isAuthorizedType(_def, "line");
}

/**
 * Transforme le retour JSON (l'object chart_oriss doit être passé à la fonction)
 * ET
 * l'inverse (pour être dans l'ordre chronologique de création)
 * @param  {Array}  chart_oriss 
 *     Array d'objets JS renvoyé par la requête "json=optim"
 * @return {Array}
 *     Toutes les valeurs du retour JSON dans un array accessible par la clé correspondante
 */
function fakeParse2(_chart_oriss) {
  if (isDebug)
    console.info("Début fonction fakeParse2");

  var resultObject = _chart_oriss.reduce(function(result, currentObject) {
    for (var key in currentObject) {
      if (currentObject.hasOwnProperty(key)) {
        //Créer la variable si elle n'existe pas
        if(!result[key])
          result[key] = [];
        result[key].push(parametres_url._mask.indexOf(key) ? Number.parseFloat(currentObject[key]) : currentObject[key]);
      }
    }
    return result;
  }, {});
  //Retourner les valeurs
  for (var key in resultObject) {
    resultObject[key].reverse();
  }

  if (isDebug)
    console.info("Fin fonction fakeParse2");
  return resultObject;
}

/**
 * Récupère le nom de la base dans l'attribut "data="
 * ET 
 * y rajoute un "s" (minuscule)
 * CE QUI correspond au nom de l'objet contenant les valeurs des Points/Axes etc... 
 * à la racine du JSON récupéré par la requête GET
 * 
 * @return {string}  le nom de la base avec un "s" en plus
 */
function getBaseName() {
  //Récupère tout AVANT ".ini" dans l'attribut "data"
  var chemin_ini = parametres_url.data.substring(0, parametres_url.data.lastIndexOf("_gestion.ini") + 12), //+12 car ".ini" = 4 caractères
      //Nom de la base AVEC "_gestion.ini"
      base_ini = chemin_ini.substring(chemin_ini.lastIndexOf("/") + 1),
      //Nom de la base SANS "_gestion.ini" et AVEC un "s" en plus
      base_nameS = base_ini.replace("_gestion.ini", "") + "s";
    
  if (isDebug) {
    console.log("Chemin: " + chemin_ini);
    console.log("Nom base: " + base_ini);
    console.warn("Nom objet JSON: " + base_nameS);
  }
  return base_nameS;
}

/**
 * Construit l'URL
 * ET
 * Effectue la requête
 * ET
 * Tente de construire le graphique en parsant les données récupérées via le GET
 * SI LE PARSE RENVOIE UNE ERREUR
 * La requête est refaite MAIS l'attribut "mask=idX,idY,..." devient "mask=X,Y,..."
 * 
 * @param {[type]} _path        [description]
 * @param {[type]} _mask        [description]
 * @param {[type]} oHighChart   [description]
 * @param {boolean} _isSecondTry
 *     Indique s'il s'agit du premier appel de cette fonction
 *     SI C'EST LE CAS
 *     le try{}catch{}, dans le catch, rappelle cette fonction en changeant la valeur de _mask
 */
function GET_chart_oriss(_path, _mask, oHighChart) {
  //GÉNÉRER L'URL
  try {
    var _url = window.location.protocol + "//" + window.location.host + "/"
              + getID_Oris() + "/"
              + _path
              + genererParamData();
  } catch (e) {
    //Erreur connue
    if (GLOBAL_ERROR_MESSAGE.length > 0) {
      //toastr.error(GLOBAL_ERROR_MESSAGE);
      console.error(GLOBAL_ERROR_MESSAGE);
      $("#root").prepend(new BootstrapAlert("danger", "Erreur:", GLOBAL_ERROR_MESSAGE).div)
    } else { //Autre erreur
      console.error(e);
      $("#root").prepend(new BootstrapAlert("danger", "Erreur:", e).div)
    }
    return false;
  }

  if (isDebug)
    console.info(_url);

  //REQUÊTE
  $.getJSON( _url , function(response) {
    //SUCCESS
    if (isDebug) {
      console.log("response");
      console.log(response);
    }

    //REGROUPER LES DONNÉES
    // !!!!!! l'objet racine ne s'appelle pas toujours "chart_oris" !!!!!!
    var nomRacine = getBaseName();
    var parsedDatas = fakeParse2(response[nomRacine]);
    if (isDebug) {
      console.log("parsedDatas");
      console.log(parsedDatas);
    }

    //VALEURS AXE X
    var xValues = [];

    //try/catch histoire d'avoir un message d'erreur plus clair
    try {
      //CATÉGORIES
      if (!parametres_url.istime || parametres_url.istime != "true") {
        console.warn("CATÉGORIES");
        if (!parsedDatas[parametres_url._mask[0]])
          throw "Le JSON ne contient pas de valeurs pour l'axe X (" + parametres_url._mask[0] + ")";
        else {
          xValues = parsedDatas[parametres_url._mask[0]];
          //MÀJ GRAPHIQUE
          oHighChart.xAxis[0].update({
            type: "category",
            //type: "linear",
            categories: xValues
          }, false); //on ne redraw() pas maintenant
        }
      } 
      //DATETIME
      else { //(parametres_url.istime && parametres_url.istime == "true")
        if (isDebug)
          console.warn("X type = DATETIME");

          xValues = parsedDatas[parametres_url._mask[0]].map(function(element){
            var _date;
            if (element.indexOf("/") > -1) //s'il y a des '/', c'est donc une Date
              _date = moment(element, "DD/MM/YYYY");  //La librairie "moment.js" s'occupe de parser la date
                                                            //qu'elle soit ISO ou "FR"
            else  //Sinon, c'est un ISO
              _date = moment(element, moment.ISO_8601);

            //
            if(_date.isValid())
              return _date;
            else
              console.error(_date, " n'est pas valide, la valeur n'est donc pas ajoutée");
          })
          //J'élimine les dates invalides (parce que map met "undefined")
          .filter(function(element){
            return typeof element != "undefined";
          });


        //MÀJ GRAPHIQUE
        oHighChart.xAxis[0].update({
          type: "datetime",
          categories: null      //IMPORTANT d'unset cette variable
        }, false); //on ne redraw() pas maintenant
      }
    } 
    catch (err) {
      $("#root").prepend(new BootstrapAlert("danger", "Erreur:", "Erreur lors du Parsing des valeurs de l'axe X.").div)
      console.error("Erreur lors du Parsing des valeurs de l'axe X: ", err);
    }
    if (isDebug) {
      console.log("xValues");
      console.log(xValues);
    }

    //DATAS
    for (var key in parsedDatas) {
      if (series[key]) {
        //CATÉGORIES => on C/C juste l'Array "data"
        series[key].data = mergeCategoryAndData(xValues, parsedDatas[key]);
        series[key].symbol = SYMBOLS_AUTO[Object.keys(series).indexOf(key) % SYMBOLS_AUTO.length];
        //series[key].color = ((parametres_url.color && parametres_url.color[key]) ? parametres_url.color[key]                    : COULEURS_AUTO[Object.keys(series).indexOf(key) % COULEURS_AUTO.length] )                    || COULEURS_AUTO[0]; //Fix à la rache pour le soucis d'initialisation de la page avec des paramètres. Initialement, les courbes n'existent pas et la couleur leur étant attribuée non plus
        //series[key].color = COULEURS_AUTO[Object.keys(series).indexOf(key) % COULEURS_AUTO.length]
                            //|| COULEURS_AUTO[0]; //Fix à la rache pour le soucis d'initialisation de la page avec des paramètres. Initialement, les courbes n'existent pas et la couleur leur étant attribuée non plus;
      }
    }

    //ADD AXIS & SERIES
    for (var i in series) {
      var indexOfSerie = Object.keys(series).indexOf(i);

      //Seulement update la série si elle existe déjà
      var serieAlreadyExists = oHighChart.get(series[i].id);
      if (isDebug)
        console.error("serieAlreadyExists ?", serieAlreadyExists!=undefined);
      if (serieAlreadyExists) {
        serieAlreadyExists.update({data: series[i].data});
        continue;
      }

      //affecter une couleur à la courbe, si c'est pas déjà le cas
      if (!series[i].color) 
        series[i].color = COULEURS_AUTO[(indexOfSerie + oHighChart.series.length) % COULEURS_AUTO.length] 
          || COULEURS_AUTO[0]

      //Pour savoir s'il faut créer un nouvel axe Y ou non
      var yAxisAlreadyExists = oHighChart.get((UNITS_DEFINITION[series[i].unit]) ? UNITS_DEFINITION[series[i].unit].toLowerCase() : series[i].unit );
      if (yAxisAlreadyExists) {
        series[i].yAxis = yAxisAlreadyExists.userOptions.index;    //Index de l'axe Y existant
        //Également ajouter la couleur de la série au label
        var ancien_titre = "";  //ou null
        if (yAxisAlreadyExists.axisTitle && yAxisAlreadyExists.axisTitle.textStr)
          ancien_titre = yAxisAlreadyExists.axisTitle.textStr;
        else //if( yAxisAlreadyExists.userOptions && yAxisAlreadyExists.userOptions.title && yAxisAlreadyExists.userOptions.title.text ){
          ancien_titre = yAxisAlreadyExists.userOptions.title.text;

        yAxisAlreadyExists.update({
          title: { 
            useHTML: true,
            text: (series[i].unit != ' ' && series[i].unit != '') ? (ancien_titre 
            + ' <i id="label-marker-' + series[i].id 
            + '" class="' + SYMBOLS_AUTO_FA_EQUIVALENT[series[i].symbol]
            + '" style="color: ' + series[i].color + '; "></i>') : ancien_titre
          },
        }, false);
      } else {  //L'axe Y n'existe pas, on le crée
        var newYaxisConfig = nouvelAxeY(series[i], indexOfSerie, oHighChart);
        //console.warn("Adding newYaxis: ", newYaxisConfig);
        
        //addAxis(options [, isX] [, redraw] [, animation])
        var newYaxis = oHighChart.addAxis(newYaxisConfig, false, false, false);  
        //Une fois l'axe ajouté, on y assigne la série
        series[i].yAxis = newYaxis.userOptions.index;
      }

      oHighChart.addSeries(series[i], false); //pas redessiner tant qu'on n'a pas fini
    }
  })
  .done(function() {
    //SUCCESS CALLBACK
    if (isDebug)
      console.log( "second success" );
    
    //HACK pour avoir le type bar
    //On dessine des 'columns'
    //MAIS
    //On inverse les axes X et Y
    //
    //ATTENTION
    //Cela s'applique à TOUT LE GRAPHIQUE
    if (parametres_url._type && parametres_url._type.indexOf('bar') > -1)
      //oHighChart.update({chart: { type: 'column', inverted: true }}, false)
      //En fait non, mais on est quand même obligé de l'appliquer à tout le graphique...
      oHighChart.update({chart: { type: 'bar', inverted: false }}, false);  //NE MARCHE PAS SI ON ENLEVE 'inverted: false'
    
/*    //
    if (parametres_url.ispolar && parametres_url.ispolar === "true" )
      oHighChart.update({chart: { polar: true/*, inverted: false*//* }}, false);  //NE MARCHE PAS SI ON ENLEVE 'inverted: false'
*/
    oHighChart.redraw();

    //refaire la requête si refresh est précisé (et est un entier valide)
    if (parametres_url.refresh && parseInt(parametres_url.refresh)) {
      setTimeout( 
        function() {
          GET_chart_oriss(parametres_url.data, parametres_url.mask, appelant);
        }, parseInt(parametres_url.refresh) * 1000); // x1000 car JS compte en ms
    }
  })
  .fail(function(err) {
    GLOBAL_ERROR_MESSAGE = "Problème lors de la requête ou de la lecture du JSON";
    console.error( GLOBAL_ERROR_MESSAGE, err );
  })
  .always(function() {
    if (isDebug)
      console.log( "complete" );
    hideLoading();
  });
}

/**
 * Fusionne les datetime et les valeurs de y
 * en s'arrêtant dès qu'on est à court de datetime 
 * (=> s'il y a plus de valeurs d'Y que de valeurs d'X, elles sont juste ignorées)
 * @param  {[type]} _xValues [description]
 * @param  {[type]} _datas   [description]
 * @return {[type]}          [description]
 */
function mergeCategoryAndData(_xValues, _datas) { //TODO: ajouter les couleurs
  if (!_xValues || !_datas) 
    return false;
  var data = [];

  //Je sais, c'est pas opti... 
  //Autant calculer X en fonction de &istime et y en fonction de &cumul
  //puis de créer les tableaux... (un seul for)

  //Cumul
  if (parametres_url.cumul && parametres_url.cumul == "true") {
    var cumulY = 0;
    for (var i=0, l=_xValues.length; i<l; ++i) {
      cumulY += Number.isNaN(_datas[i]) ? 0 : _datas[i]
      if (parametres_url.istime && parametres_url.istime == "true")
        data.push({
          x: _xValues[i]._d.getTime(),  //convertir l'objet Moment en timestamp
          y: Number.isNaN(_datas[i]) ? null : cumulY   //Important de parse en nombre (erreur 14 highcharts, sinon)
        });
      else 
        data.push({
          name: _xValues[i],  //convertir l'objet Moment en timestamp
          y: Number.isNaN(_datas[i]) ? null : cumulY   //Important de parse en nombre (erreur 14 highcharts, sinon)
        });
    }
  if (isDebug)
        console.warn("data", data)
  return data;
  }


  for (var i=0, l=_xValues.length; i<l; ++i) {
    if (parametres_url.istime && parametres_url.istime == "true")
      data.push({
        x: _xValues[i]._d.getTime(),  //convertir l'objet Moment en timestamp
        y: Number.isNaN(_datas[i]) ? null : _datas[i]   //Important de parse en nombre (erreur 14 highcharts, sinon)
      });
    else 
      data.push({
        name: _xValues[i],  //convertir l'objet Moment en timestamp
        y: Number.isNaN(_datas[i]) ? null : _datas[i]   //Important de parse en nombre (erreur 14 highcharts, sinon)
      });
  }
  if (isDebug)
        console.warn("data", data)
  return data;
}

/**
 * Génère la valeur de l'attribut 'data=' pour la requête GET
 * en fusionnant tous les paramètres de l'URL
 * HORMIS les noms réservés
 * - data
 * 
 * @return {string} 
 *     la valeur de l'attribut data
 */
function genererParamData () {
  var toutData = "?json=true";
  for(var i in parametres_url) {
    if (typeof parametres_url[i] === "string" && i != "data")
      toutData += "&" + i + "=" + parametres_url[i];
  }
  return toutData;
}


//INUTILE
//INUTILE
//INUTILE
//INUTILE
/**
 * Génère la valeur de l'attribut 'data=' pour la requête GET
 * en fusionnant tous les paramètres de l'URL
 * HORMIS les noms réservés
 * - data
 * 
 * @return {string} 
 *     la valeur de l'attribut data
 */
function genererParamDataSansReserves () {
  var toutData = "?json=true";
  for(var i in parametres_url) {
    if (typeof parametres_url[i] === "string" 
        && i != "data"
        && i != "title"
        && i != "name"
        && i != "type"
        && i != "color"
        && i != "istime"
        && i != "unit")
      toutData += "&" + i + "=" + parametres_url[i];
  }
  return toutData;
}


/////////////////
/// PROTOTYPE ///
/////////////////
/**
 * Remplace toutes les occurences d'une chaine de caractères dans une autre
 * @param  {string} search      
 *     texte que l'on souhaite retirer
 * @param  {string} replacement 
 *     ce par quoi on veut le remplacer
 * @return {string}             
 *     la chaine de caractère moins les éléments supprimés
 */
String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};