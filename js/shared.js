//////////////
/// Classe ///
//////////////
var Serie = (function(idSerie) {
  return function(idSerie) { 
    this.id               = idSerie;
    this.unit             = " ";  //unité vide = axe Y sans nom (car définit dans unit.js)
    this.name             = " ";
    this.type             = "line";
    //data[<String>] = {y: <Integer>, color: <String>}
    this.data             = [];
  };
})();
var BootstrapAlert = (function(_type, _strong, _message) {
  return function(_type, _strong, _message) { 
    this.div = document.createElement("div");
    this.div.className = 'alert alert-'+ _type;
    this.div.innerHTML = '<strong>' + _strong + '</strong> ' + _message  
    + ' <button type="button" class="close" data-dismiss="alert" aria-label="Close">'
    +  '<span aria-hidden="true">&times;</span>  </button>';
  };
})();


//////////////////////////
/// FONCTIONS COMMUNES ///
//////////////////////////
/**
 * Transforme les données récupérées dans le JSON (ancien) 
 * en quelque chose de lisible pour HighCharts
 * @param  {series.points}  responseData  
 *     tableau de données du JSON obtenu en réponse de la requête
 * @return {Array}
 *     un tableau lisible par HighCharts
 */
function fakeParseDataSerie(responseData){
  var i = responseData.length;
  //Preprocessed data array
  var ideal = [],
      regExp = /\(([^)]+)\)/;

  while( i-- ){
    if (CHART_SETTINGS.xAxisType == "datetime") {
      var matches = regExp.exec(responseData[i].Key);
      if (matches)  //Cas datetime
        ideal.push([parseInt(matches[1]), responseData[i].Value]); //[0] garde les parenthèses, or on ne les veut pas --> [1]
    } else {
      ideal.push({
        name  : responseData[i].name || responseData[i].Key,
        y     : responseData[i].Value,
        color : responseData[i].color || undefined
      });
    }
  }
  //On inverse le tableau car le JSON d'Oris est créé à l'envers
  return ideal.reverse();
}

/**
 * Crée un nouvel axe Y à partir des données d'une série (donc un Point, enfin presque)
 * Son unité, label, etc... correspondra à celui de la série
 * @param  {API Object} serie  Objet série HighCharts
 * @return {[type]}       [description]
 */
function nouvelAxeY(serie, _indexOfSerie, oHighChart, afficherPastilles) {
  afficherPastilles = afficherPastilles || false;

  //|| COULEURS_AUTO[(_indexOfSerie + oHighChart.series.length) % COULEURS_AUTO.length] 
  var tmp_couleur = serie.color || COULEURS_AUTO[0];
  var tmp_symbol = serie.symbol || SYMBOLS_AUTO[0];
  

  var newY = {
    title: { 
      useHTML: true,
      text: (serie.unit != ' ' && serie.unit != '') ? ((UNITS_DEFINITION[serie.unit] || serie.unit || '')
        + ' <i id="label-marker-' + serie.id 
        + '" class="' + SYMBOLS_AUTO_FA_EQUIVALENT[tmp_symbol] 
        + '" style="color: ' + tmp_couleur + '; " ></i>') : ''
    },
    // convertir s'il s'agit d'un groupe (Binaire, Puissance, Pression, ...)
    id: (UNITS_DEFINITION[serie.unit]) ? UNITS_DEFINITION[serie.unit].toLowerCase() : serie.unit,   //Ceci a pour effet de regrouper les "m" et les "mm" dans un axe "Distance" (ce qui peut poser problème si y a 3m et 40mm, large différence d'échelle)
              //On peut sinon mettre directement "serie.unit" mais on se retrouve avec deux axes "Distance"
    lineWidth: 1,
    //label: { format: "{value}"+serie.unit },
    opposite: (appelant.yAxis.length % 2 == 1)
  }
  if( newY.opposite )
    newY.className = "is-opposite";
  return newY;
}

///////////////////////////////
//////////// Maths ////////////
///////////////////////////////
function standardDeviation(values){
  var avg = moyenne(values);

  var squareDiffs = values.map(function(value){
  if( value == null || value == undefined || value == "" ){
    return null;
  }
    if( typeof value == "number" ){
      var diff = value - avg;
      var sqrDiff = diff * diff;
      return sqrDiff;  
    } else if ( typeof value.y == "number" ){
      var diff = value.y - avg;
      var sqrDiff = diff * diff;
  } else {
      return null;
    }
  });
  
  var avgSquareDiff = moyenne(squareDiffs);

  var stdDev = Math.sqrt(avgSquareDiff);
  return stdDev;
}
function average(data){
  var sum = data.reduce(function(sum, value){
    return sum + value;
  }, 0);

  var avg = sum / data.length;
  return avg;
}
/**
 * Fonctions "basiques" mais gérant les valeurs null / "" (vide) / undefined
 * La moyenne est calculée en fonction du nombre total de valeurs "valides" (s'il y a 3 valeurs vides => total/.length-3)
 * @param  {Array} tableau de valeurs pouvant contenir des trous
 **/
function moyenne(tableau){
  var sum = 0,
    vides = 0;

  var i=tableau.length;
  while( i-- ){
    //Ignorer les valeurs nulles ou vides
    if( tableau[i] == null || tableau[i] == undefined || tableau[i] == "" ){
      vides += 1;
      continue;
    }
    //Cas normal, la valeur est un nombre
    if(typeof tableau[i] == "number"){
      sum += tableau[i];
    } else {  
      //Cas marker (objet)
      if(typeof(tableau[i].y) == "number"){
        sum += tableau[i].y;
      } else {
        vides += 1;
      }
    }
  }

  var moy = sum / (tableau.length - vides);
  return moy;
}
function min(tableau, returnIndex){
  returnIndex = returnIndex || false;
  var tmp = Math.min.apply(null, tableau);
  if(!isNaN(tmp)){
    return tmp;
  } else {
    var _min=Number.POSITIVE_INFINITY;
    var index = -1;
    var i=tableau.length;
    while(i--){
      if(tableau[i]==null || tableau[i]==undefined || tableau[i]==""){
        continue;
      } 
      if(typeof tableau[i] == "object"){
        var x = tableau[i].y;
      } else {  //regular number
        var x = tableau[i];
      }
      if(typeof x == "number" && _min > x){
        _min = x;
        index = i;
      }
    }
    if(returnIndex){
      return index;
    }
    return _min;
  }
}
function max(tableau, returnIndex){
  returnIndex = returnIndex || false;
  var tmp = Math.max.apply(null, tableau);
  if(!isNaN(tmp)){
    return tmp;
  } else {
    var _max = Number.NEGATIVE_INFINITY;
    var index = -1;
    var i = tableau.length;
    while( i-- ){
      if(tableau[i] == null || tableau[i] == undefined || tableau[i] == ""){
        continue;
      } 
      if(typeof tableau[i] == "object"){
        var x = tableau[i].y;
      } else {  //regular number
        var x = tableau[i];
      }
      if(typeof x == "number" && _max < x){
        _max = x;
        index = i;
      }
    }
    if(returnIndex){
      return index;
    }
    return _max;
  }
}

////////////////////////////
////////// Divers //////////
////////////////////////////
/**
 * Renvoie l'hostname de l'url, c-à-d l'IP sans le port ou www.enoleo.com 
 * Initialise le booléen indiquant si l'utilisateur est en local ou non
 * @return {string} ip sans le port ou 'www.enoleo.com'
 **/
function getIP(){
  USER_INFOS.ip = window.location.hostname;
  USER_INFOS.is_local = USER_INFOS.ip == "www.enoleo.com" ? true : false;
  return USER_INFOS.ip;
}

/**
 * Renvoie l'ID Oris en "splittant" l'url à chaque slash ("/")
 * @return {string} id Oris
 */
function getID_Oris(){
  var path = window.location.pathname; // "/id-000192.168.1.74424011-0/img/vegarw/chart/index.html"
  if (path.split("/")[1].indexOf("id-") >= 0) {
    USER_INFOS.id_oris = path.split("/")[1];  //[0] est une chaine vide car le pathname commence par un "/"
    return USER_INFOS.id_oris;
  } else
    throw "Erreur lors de la récupération de l'ID-Oris";
}

/*****************
      Loading
 *****************/
function showLoading(){
  document.getElementById("loading-overlay").style.display = "inline";
}
function hideLoading(){
  document.getElementById("loading-overlay").style.display = "none";
}