/////////////////////////////////////////////////////
/////////////////// Monitoring //////////////////////
////////////////////////////////////////////////////
function monitoringForChart(oHighChart){
  var containerId = oHighChart.renderTo.id
  if(monitoringEstEnCours[containerId] && monitoringEstEnCours[containerId] == true ){
    console.error("monitoringEstEnCours." + oHighChart.renderTo.id + "= true");
    return false;
  }
  if( typeof monitoringEstEnCours[containerId] == "undefined" )
    monitoringEstEnCours[containerId] = { };

  monitoringEstEnCours[containerId].enCours = true;
  var _now = Date.now();
  //loop à travers les yAxis existants
  var i = oHighChart.yAxis.length;
  while ( i-- ){
    // console.log("axeY: " + i);
    var j = oHighChart.yAxis[i].series.length;
    //loop à travers les séries existantes
    while ( j-- ){
      // console.log("serie:  " + j);
      //Ajoute à Date.now() la nouvelle valeur d'un Point (qui est la même que l'ancienne si elle n'a pas changé)
        if( isDebug ){
          console.log( "MaJ courbe " + oHighChart.yAxis[i].series[j].userOptions.id );
          console.log( "oHighChart.yAxis[" + i + "].series[" + j + "].addPoint({ x:" 
          + _now + ", y: " 
           + pointss({ id0: oHighChart.yAxis[i].series[j].userOptions.id }).first().id23 
           + " })" );
        }
      oHighChart.yAxis[i].series[j].addPoint({ 
        x: _now, 
        y: parseFloat( pointss({ id0: oHighChart.yAxis[i].series[j].userOptions.id }).first().id23 )
      }, false, true); //{ objPoint, redraw, shift } Shift fait disparaitre la toute première  valeur !
      //Possibilité de shift seulement si l'on a plus de 5 minutes d'intervalle observable
    }
  }
  oHighChart.redraw();
  monitoringEstEnCours[containerId].enCours = false;
}

/**
 * Refait la requête permettant la MaJ des valeurs (id23) de la BD locale de Points
 * via un setInterval
 * Normalement, c'est fait dans la page principale
 * @param  {int}     intervalEnMs    délais minimal entre deux requête, en millisecondes
 **/
function startAutoGET_Pointss(intervalEnMs) {
  intervalEnMs = intervalEnMs || 3000; //en ms
  startAutoGET_PointssSetInterval = window.setInterval(GET_Pointss, intervalEnMs);
}
/**
 * Met un terme au setInterval de Get_Pointss()
 **/
function stopAutoGET_Pointss() {
  if( startAutoGET_PointssSetInterval )
    window.clearInterval(startAutoGET_PointssSetInterval);
}
function startMonitoringForChart(oHighChart){
  var containerId = oHighChart.renderTo.id;

  //Démarrer la récupération continue des valeurs instantanées si ça n'est pas déjà le cas
  if( typeof( startAutoGET_PointssSetInterval ) == "undefined" )
    startAutoGET_Pointss();
  if(!monitoringEstEnCours[containerId])
    monitoringEstEnCours[containerId] = { };

  desactiverTOUT(oHighChart);
  monitoringEstEnCours[containerId].intervalleId = window.setInterval(function() { 
    monitoringForChart(oHighChart); 
  }, 5000);
  //On veut mettre la date à jour toutes les minutes, pas toutes les 60s
  monitoringEstEnCours[containerId].autoUpdateDateFin = setTimeout(function(){
    //Mettre à jour la date de fin chaque minute
    var _dateTimePickerFin = $("#" + UI_IDs.quickRequestDatePickerFin + containerId);
    updateDateTimePickerToNow(_dateTimePickerFin);
    monitoringEstEnCours[containerId].autoUpdateDateFin = window.setInterval(function(){
      updateDateTimePickerToNow(_dateTimePickerFin);
    }, 60 * 1000);
  }, (60 - moment().seconds()) * 1000 );
}
function stopMonitoringForChart(oHighChart){
  var containerId = oHighChart.renderTo.id;
  if( monitoringEstEnCours[containerId] && monitoringEstEnCours[oHighChart.renderTo.id].intervalleId 
      && monitoringEstEnCours[containerId].autoUpdateDateFin ){
    deverrouillerQuickRequestWidget(containerId);
    highlightTOUT(oHighChart);
    
    window.clearInterval(monitoringEstEnCours[containerId].intervalleId);
    window.clearInterval(monitoringEstEnCours[containerId].autoUpdateDateFin);
    //Juste pour être clair, mais en réalité surtout inutile
    monitoringEstEnCours[containerId].intervalleId = -1;
    monitoringEstEnCours[containerId].autoUpdateDateFin = -1;
  }
}
function toggleMonitoringForChartById(event, containerId){
  //Est-ce qu'on lance ou arrête le monitoring
  var isChecked = event.checked;
  //Graphique retrouvé via son ID
  var oHighChart = $("#"+containerId).highcharts();
  if( !oHighChart )
    return false;
  if(isChecked)  //On le lance
    startMonitoringForChart(oHighChart);
  else  //On l'arrête
    stopMonitoringForChart(oHighChart);
}
/**
 * Dé/bloque "visuellement" l'utilisation de certains éléments (comme les sélecteurs de date)
 * lors du début/fin du monitoring
 * @param  {string}   containerId   ID du graph concerné par le monitoring
 **/
function verrouillerQuickRequestWidget(containerId){
  //disable datetimepicker & slider & select & btn
  var _quickRequestContainer = $("#" + UI_IDs.quickRequestContainer + containerId);
  _quickRequestContainer.addClass("is-monitoring");
  //DateTimePickers
  _quickRequestContainer.find("#" + UI_IDs.quickRequestDatePickerDebut + containerId).data("DateTimePicker").disable();
  _quickRequestContainer.find("#" + UI_IDs.quickRequestDatePickerFin + containerId).data("DateTimePicker").disable();
  //Slider
  _quickRequestContainer.find("input#" + UI_IDs.quickRequestSliderId + containerId).slider("disable");
  //Select  
  _quickRequestContainer.find("select#" + UI_IDs.quickRequestPeriodeSelect + containerId).attr("disabled", true);
  //Button
  _quickRequestContainer.find("button.btn").attr("disabled", true);
  //Ajouter la classe au graphique (afin de ne plus pouvoir ajouter de Points)
  $("#" + containerId).addClass("is-monitoring");

}
function deverrouillerQuickRequestWidget(containerId){
  //enable datetimepicker & slider & select & btn
  var _quickRequestContainer = $("#" + UI_IDs.quickRequestContainer + containerId);
  _quickRequestContainer.removeClass("is-monitoring");
  //DateTimePickers
  _quickRequestContainer.find("#" + UI_IDs.quickRequestDatePickerDebut + containerId).data("DateTimePicker").enable();
  _quickRequestContainer.find("#" + UI_IDs.quickRequestDatePickerFin + containerId).data("DateTimePicker").enable();
  //Slider
  _quickRequestContainer.find("input#" + UI_IDs.quickRequestSliderId + containerId).slider("enable");
  //Select  
  _quickRequestContainer.find("#" + UI_IDs.quickRequestPeriodeSelect + containerId).removeAttr("disabled");
  //Button
  _quickRequestContainer.find("button.btn").attr("disabled", false);
  //Enlever la classe au graphique (afin de pouvoir ajouter de Points)
  $("#" + containerId).removeClass("is-monitoring");
}

/**
 * Met automatiquement le DateTimePicker à l'heure (sinon, on serait obligé de raffraichir la page parce que la date Max du picker est la seconde à laquelle la page s'est lancée...)
 * @param  {jQuery Object}   _dateTimePicker  DateTimePicker que l'ont veut mettre à jour,
 *                                            sélectionné via jQuery 
 **/
function updateDateTimePickerToNow(_dateTimePicker){
  _dateTimePicker.data("DateTimePicker").options({
    maxDate: moment().add(5, "seconds") //on se laisse une petite marge au cas où...
  });
  _dateTimePicker.data("DateTimePicker").date(moment());
}